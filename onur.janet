#!/usr/bin/env janet

# Onur is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

(import ./src/commands :as commands)

(def args (dyn :args))

(def usage
  "Usage"
``` [action]

  Actions:
    help                                  print this usage information
    archive <projects>                    archive projects
    grab                                  grab all projects```)

(def action (get args 1))
(def options (drop 2 args))

(def actions ["help" "archive" "grab"])

(if (and (nil? (find-index |(= action $) actions))
         (not (nil? action)))
  (do
    (print "'" action "' is not a supported action.\n")
    (print "onur" usage)
    (os/exit 1)))

(if (or (nil? action)
        (empty? action))
  (print "onur" usage)
  (case action
    "help" (print "onur" usage)
    "archive" (commands/archive ;options)
    "grab" (commands/grab)))
